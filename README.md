# Инструкция по запуску

В этом задании я обошёлся только системными библиотеками, чтобы отказаться от сборщиков. Сперва компилим:

    javac AlfaSort.java

Теперь запускаем:

    java AlfaSort -f ../numbers.txt

Для сортировки в обратном порядке нужно указать опцию `-d`:

    java AlfaSort -f ../numbers.txt -d

Или:
    
    java AlfaSort -d -f ../numbers.txt
