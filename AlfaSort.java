import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class AlfaSort {
    public static void main (String[] args) {
        boolean desc = false;
        
        int fileOptionIndex = Arrays.asList(args).indexOf("-f");
        if (fileOptionIndex < 0) {
            System.out.println("Please specify file name");
            return;
        }
        
        String fileName = args[fileOptionIndex + 1];

        int descOptionIndex = Arrays.asList(args).indexOf("-d");
        if (descOptionIndex >= 0) {
            desc = true;
        }
    
        try {
            List<Integer> numbers = parseNumbers(readFile(fileName));
            if (numbers != null && numbers.size() > 0) {
                System.out.println(sortNumbers(numbers, desc));
            } else {
                System.out.println("Nothing to Sort");
            }
        
        } catch (IOException e) {
            System.out.println("File not found");
        }
    }
    
    private static List<Integer> sortNumbers(List<Integer> numbers, Boolean desc) {
        Collections.sort(numbers);
        if (desc) {
            Collections.reverse(numbers);
        }
        return numbers;
    }
    
    private static List<Integer> parseNumbers(List<String> lines) {
        List<Integer> numbers = new ArrayList<Integer>();
        
        for (String line: lines) {
            for (String number: line.split(",")) {
                try {
                    numbers.add(Integer.parseInt(number.trim()));
                } catch (NumberFormatException e) {
                    System.out.println(String.format("\"%s\" isn't an integer", number));
                }
            }
        }
        return numbers;
    }
    
    private static List<String> readFile(String fileName) throws IOException {
        List<String> lines = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new FileReader(fileName));
    
        String line;
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
    
        br.close();
        return lines;
    }
}
